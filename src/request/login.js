import myAxios from './axios'
export function login(paramsList) {
  return myAxios({
    url: '/api/login',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: paramsList
  });
}